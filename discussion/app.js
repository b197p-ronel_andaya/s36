const express = require('express');
const mongoose = require('mongoose');

const taskRoutes = require('./routes/taskRoutes');

const app = express();

const port = 3001;

mongoose.connect(`mongodb+srv://motifaithed:R0n3l0630@zuittbootcampb197-p.m5646t2.mongodb.net/S36?retryWrites=true&w=majority`,{
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection;

db.on('error',()=>{
    console.error('connection error');
})
db.once('open',()=>{
    console.log('Connected to MongoDB');
})

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/tasks', taskRoutes);

app.listen(port,()=>{
    console.log(`server is now listening on port: ${port}`);
})