const { isObjectIdOrHexString } = require("mongoose");
const Task = require("../models/Task.model");

async function createTask(req, res) {
  try {
    const findDuplicate = await Task.find({ name: req.body.name });

    if (findDuplicate !== null && findDuplicate.length > 0) {
      return res.send("Duplicate Task Found!");
    }

    const task = new Task({
      name: req.body.name,
    });

    const result = await task.save();
    return res.send(result);
  } catch (error) {
    console.log(error);
    return res.send(error);
  }
}

async function getTasks(req,res){
    try{
        const result = await Task.find({});
        res.send(result);
    }catch(error){
        return res.send(error);
    }
}

async function getTask(req,res){
    try{
        const result = await Task.findById(req.params.id);
        if(result){
           return res.send(result);
        }
        return res.send('No task associated with that id');
    }catch(error){
        return res.send(error);
    }
}

async function updateTask(req,res){
    try{
        const task = await Task.findById(req.params.id);
        if(task){
            task.name = req.body.name;
            const update = await task.save();
            return res.send(update);
        }else{
            return res.send('no task associated with that id!');
        }
    }catch(error){
        console.log(error);
        return res.send(error);
    }
}

async function setStatusComplete(req,res){
    try{
        const task = await Task.findById(req.params.id);
        if(task){
            task.status = req.body.status;
            const update = await task.save();
            return res.send(update);
        }else{
            return res.send('no task associated with that id!');
        }
    }catch(error){
        console.log(error);
        return res.send(error);
    }
}

async function deleteTask(req,res){
    try{
        const taskDelete = await Task.findByIdAndRemove(req.params.id);
        if(taskDelete){
           return res.send(taskDelete);
        }

        return res.send('Something went wrong. Please try again later');

    }catch(error){
        console.log(error);
        res.send(error);
    }
}

module.exports = {
  createTask: createTask,
  getTasks: getTasks,
  getTask: getTask,
  updateTask: updateTask,
  deleteTask: deleteTask,
  setStatusComplete: setStatusComplete
};
